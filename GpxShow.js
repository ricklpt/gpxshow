﻿var directionsDisplay;
var directionsService = new google.maps.DirectionsService();
var map;


$(function () {
	directionsDisplay = new google.maps.DirectionsRenderer();
	var mapOptions = {
		center: new google.maps.LatLng(0, 0),
		zoom: 1,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	map = new google.maps.Map(document.getElementById("map-canvas"),
			mapOptions);
	directionsDisplay.setMap(map);

	$.ajax({
		type: "GET",
		url: "waypoints.xml",
		dataType: "xml",
		success: function (xml) {
			var wpts = $(xml).find('trkpt');
			var waypts = [];
			var previouspoint;
			var previoustime;
			var totallenght = 0;
			var totaltime = 0;
			var d1 = [];
			var d2 = [];
			var maxspeed = 0;
			var minheight = 0;
			var maxheight = 0;
			var bounds = new google.maps.LatLngBounds();


			for (var i = 0; i < wpts.length; i++)
			{
				var that = $(wpts[i]);
				var lat = that.attr('lat');
				var long = that.attr('lon');
				var elevation = that.children("ele").text();
				var time = new Date(that.children("time").text());				
				var speed;
				var myLatlng = new google.maps.LatLng(lat, long);

				minheight = Math.min(minheight, elevation);
				maxheight = Math.max(maxheight, elevation);

				waypts.push(myLatlng);
				bounds.extend(myLatlng);

				var thispoint = new LatLon(Geo.parseDMS(lat), Geo.parseDMS(long));

				if (previouspoint != undefined)
				{
					var distance = parseFloat(thispoint.distanceTo(previouspoint)); // in km
					var timediff = (time.getTime() - previoustime.getTime()) / 1000; // in seconds

					speed = ((distance * 1000) / timediff) * 3.6;
					totaltime = totaltime + timediff;

					maxspeed = Math.max(maxspeed, speed);

					totallenght = totallenght + distance;
				}

				previouspoint = thispoint;
				previoustime = time;

				d1.push([totallenght, elevation]);
				d2.push([totallenght, speed]);
			}

			var trackLog = new google.maps.Polyline
		({
			path: waypts,
			strokeColor: "#FF0000",
			strokeOpacity: 0.5,
			strokeWeight: 4
		});

			trackLog.setMap(map);



			map.fitBounds(bounds);

			$.plot("#elevation-placeholder", [d1]);
			$.plot("#speed-placeholder", [d2]);
			$('#maxspeed').text(maxspeed);
			$('#avgspeed').text((totallenght * 1000 / totaltime) * 3.6);
			$('#maxelevation').text(maxheight);
			$('#minelevation').text(minheight);
		}
	});


});

